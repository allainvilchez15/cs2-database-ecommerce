const mongoose = require('mongoose');



const userSchema = new mongoose.Schema({
		firstName: {
			type : String,
			required: true,
		},
		lastName: {
			type : String,
			required: true,
		},
		email: {
			type : String,
			required: true,
		},
		password: {
			type : String,
			required: true,
		},
		mobileNo: {
			type : String,
			required: true,
		},
		accessLevel: {
			type: String,
			default: "basic" || 0,
			enum: ["basic" || 0, "employee" || 0, "admin" || 1]
		},
		isAdmin: {
			type: Boolean,
			default: false
		},



});




module.exports = mongoose.model('User', userSchema);