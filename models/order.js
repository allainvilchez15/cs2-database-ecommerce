const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({

		userId: {
					type: String,
					required: true,
		},
		totalAmount: {
					type: String,
		},
		 orderStatus: {
			type: String,
					default: "On hold",
					enum: ["On hold", "Being Process", "Completed", "Cancelled"]
		},
		orderCreatedOn: { 
			type: Date, default: Date.now()
		},
		isActive: {
			type: Boolean,
			default: false
		},
		products: [
      {
      	_id: false,
        productId: String,
        productName: String,
        price: Number,
        quantity: Number,
        addedOnCart: { type: Date, default: Date.now()},
       
      }
    ],
	/*	purchasedOn: {
					type: Date,
					default: Date.now()
		},
		totalAmount: {
					type: String,
					required: true,
		},
		orderStatus: {
			type: String,
					default: "Being Process",
					enum: ["Being Process", "Completed", "Cancelled"]
		},
*/
});

module.exports = mongoose.model('Order', orderSchema);