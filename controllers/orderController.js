const User = require('../models/user');
const Product = require('../models/product');
const Order = require('../models/order');




//CREATEORDER

/*TEST123*/

module.exports.createOrder = async (data) => {

		const inStock = await Product.findById(data.productId) 
		if (inStock.isActive !== false) 
		{let order = new Order({

				userId: data.userId,
				totalAmount: data.quantity * inStock.price,
				products: 
				[
     			 	{
			        productId: data.productId,
			        productName: inStock.name,
			        price: inStock.price,
			        quantity: data.quantity,			
			        }
			    ],
				
				

			});


/*console.log(order)*/


	return order.save()
	.then((user, err) => {
		return (err) ? false : true/*JSON.stringify(`Order Complete! "Status": Being Processed!`)*/;
	});
} else {
	return JSON.stringify(`Product with ID "${data.productId}" either not available or currently out of stock!`)


};

};





/*GETUSERORDER*/
module.exports.getUsersOrder = async (data) =>{
	let name = await User.find({"_id": data.userId});



let userOrder = await Order.find({ "userId": data.userId }, {/*'userId': 1, 'totalAmount': 1, 'orderStatus': 1, 'products.productId': 1, 'products.purchasedOn': 1, 'products.productName': 1,'products.price': 1, 'products.quantity': 1, 'orderCreatedOn': 1, 'addedOnCart': 1*/ });
	if(userOrder.length <= 0 ) {
		return false/*`${name[0].firstName}'s current orders List is "Empty!" `*/;
	} else {
		return (userOrder);
	}



	};

/*GETALLORDERSASMINONLY*/
module.exports.getAllOrders = async() => {

	return Order.find({}, {/*'userId': 1, 'totalAmount': 1, 'orderStatus': 1, 'products.productId': 1, 'products.purchasedOn': 1, 'products.productName': 1,'products.price': 1, 'products.quantity': 1, 'orderCreatedOn': 1*/})
		.then((result, error) => {
		if(result){
			return result;
		} else {
			return console.log(error);
		}		
	});
};


/*UPDATEPRODUCT*/

module.exports.updateOrder = async (orderId, updateProduct) => {
const inStock = await Product.findById(updateProduct.productId) 
if (inStock.isActive !== false) ;
let findOrder =  await Order.findById(orderId)
/*console.log(findOrder);*/
if(findOrder === null) {
		return 'OrderId Invalid';
	} 
let currentTotal = inStock.price * updateProduct.quantity;
let updatedTotal = parseInt(findOrder.totalAmount) + parseInt(currentTotal);
let updateOrder = await Order.findOneAndUpdate(


	{_id: findOrder.id},

	{$push: {
		products: {
          $each: [ { productId: updateProduct.productId, productName: inStock.name, price: inStock.price, quantity: updateProduct.quantity }, ],
        
       }
	}
});

let updateAmountTotal = await Order.findByIdAndUpdate(orderId, {$set: {totalAmount: updatedTotal}})
.then((result, error) => {
	if (error){
			console.log(error);
			return false;
	}  	
});
return (true /*`Order with ID '${orderId}' is UPDATED!`*/);
};


/*GETSPICIFICORDER CS3*/
module.exports.getUsersSpecificOrder = (findId) => {

	return Order.findById(findId, {/*'_id': 0, 'name': 1, 'description': 1, 'price': 1, 'isActive': 1*/})
		.then((result, error) => {
		if(result){
			return result;
		} else {
			return console.log(error);
		}		
	});
	};




module.exports.getSpecificOrder = (findId) => {

	return Order.findById(findId, {/*'_id': 0, 'name': 1, 'description': 1, 'price': 1, 'isActive': 1*/})
		.then((result, error) => {
		if(result){
			return result;
		} else {
			return console.log(error);
		}		
	});
	};