const bcrypt = require('bcrypt');
const User = require('../models/user');
const Order = require('../models/order');
const Product = require('../models/product');


module.exports.adminCreatePage = async (params) => {

	/*EMAILCHECKING*/
	const emailExist = await User.findOne({email: params.email});
	if(emailExist) return JSON.stringify("Email Already Exists in Db!");
	/*EMAILCHECKING*/


	/*REGISTERNEWUSER*/
	let user = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10),
		accessLevel: "admin",
		isAdmin: true



	});
	return user.save()
	.then((user, err) => {
		return (err) ? false : true/*JSON.stringify("USER CREATED")*/;
	});
	
};

module.exports.getAllUsers = () => {

	return User.find({ /*"isActive": true*/}, {/*'_id': 1, 'name': 1, 'description': 1, 'price': 1, 'isActive': 1, 'productImage': 1, 'category': 1*/})
		.then((result, error) => {
		if(result){
			return result;
		} else {
			return console.log(error);
		}		
	});
};

//DETELE USER CS3

module.exports.deleteUser = (userId) => {
//1. look for the task with the correspoind id
//2. used the remove() to delete that specific task from the database

return User.findByIdAndRemove(userId)
.then((removedTask, error) => {
	if(error) {
		console.log(error);
		return false;
	} else {
		return (true/*'USER Deleted'*/);
	}
});

};


module.exports.deleteProduct = (productId) => {
//1. look for the task with the correspoind id
//2. used the remove() to delete that specific task from the database

return Product.findByIdAndRemove(productId)
.then((removedTask, error) => {
	if(error) {
		console.log(error);
		return false;
	} else {
		return (true/*'USER Deleted'*/);
	}
});

};



/*SetArchiveProduct*/
module.exports.ArchiveOrder = (findId) => {
return Order.findByIdAndUpdate(findId, {$set: {'orderStatus': 'Ongoing', 'isActive': true}})
.then((result, error) => {
	if (error){
			console.log(error);
			return false;
	} else {
		return true/*JSON.stringify((`'${findId}'s accountLevel set to 'Admin'!`))*/;
	}
});
};
/*SetArchiveProduct*/
/*SetProductActive CS3*/
module.exports.ReArchiveOrder = (findId) => {
return Order.findByIdAndUpdate(findId, {$set: {'orderStatus': 'On hold', 'isActive': false}})
.then((result, error) => {
	if (error){
			console.log(error);
			return false;
	} else {
		return true/*JSON.stringify((`'${findId}'s accountLevel set to 'Admin'!`))*/;
	}
});
};
/*SetProductActive CS3*/

/*DELTEORDRE CS3*/
module.exports.deleteOrder = (orderId) => {
//1. look for the task with the correspoind id
//2. used the remove() to delete that specific task from the database

return Order.findByIdAndRemove(orderId)
.then((removedTask, error) => {
	if(error) {
		console.log(error);
		return false;
	} else {
		return (true/*'USER Deleted'*/);
	}
});

};
