const Product = require('../models/product');
const User = require('../models/user');
const Test = require('../models/cpuProduct')



/*CREATECPU FOR CS3*/
module.exports.createProductCPU = async(params) => {


	let productCheck = await Product.findOne({ name: params.name})
	if(productCheck) return JSON.stringify("Product Already Exists in Db!");

	let cpuProduct = new Product({

		name: params.name,
		highlightText: params.highlightText,
		description: params.description,
		category: params.category,
		price: params.price,


	}); console.log(cpuProduct)
	return cpuProduct.save()
	.then((user, err) => {
		return (err) ? false : JSON.stringify(`${params.name} added to the DB!`);
	});
};



/*CREATEMOBO FOR CS3*/
module.exports.createProductMOBO = async(params) => {
	/*	console.log(data)*/
/*
	let productCheck = await Product.findOne({ name: params.name})
	if(productCheck) return JSON.stringify("Product Already Exists in Db!");
*/
	let product = new Product({

		name: params.name,
		highlightText: params.highlightText,
		description: params.description,
		price: params.price,
		category: params.category


	}); console.log(product)
	return product.save()
	.then((user, err) => {
		return (err) ? false : JSON.stringify(`${params.name} added to the DB!`);
	});
};



/*CREATEGPU FOR CS3*/
module.exports.createProduct = async(data, params) => {
	/*	console.log(data)*/

	let productCheck = await Product.findOne({ name: params.name})
	if(productCheck) return JSON.stringify("Product Already Exists in Db!");

	let product = new Product({

		name: params.name,
		highlightText: params.highlightText,
		description: params.description,
		price: params.price,
		category: params.category,
		productImage: data,

	}); console.log(product)
	return product.save()
	.then((user, err) => {
		return (err) ? false : JSON.stringify(`${params.name} added to the DB!`);
	});
};



module.exports.setProductImage = async(data, params) => {
	/*	console.log(data)*/

	let productCheck = await Product.findOne({ name: params.name})
	if(productCheck) return JSON.stringify("Product Already Exists in Db!");

	let product = new Product({

	
		productImage: data,

	}); console.log(product)
	return product.save()
	.then((user, err) => {
		return (err) ? false : JSON.stringify(`${params.name} added to the DB!`);
	});
};







module.exports.createProduct = async(data, params) => {
	/*	console.log(data)*/

	let productCheck = await Product.findOne({ name: params.name})
	if(productCheck) return JSON.stringify("Product Already Exists in Db!");

	let product = new Product({

		name: params.name,
		highlightText: params.highlightText,
		description: params.description,
		price: params.price,
		category: params.category,
		productImage: data,

	}); console.log(product)
	return product.save()
	.then((user, err) => {
		return (err) ? false :  JSON.stringify(`${params.name} added to the DB!`);
	});
};









/*GETALLPRODUCT*/ /*CHANGETOACTIVEONLY FOR CS3*/ /*REMOVE REQUIRED FOR LOGIN FOR CS3*/
module.exports.getAllProduct = () => {

	return Product.find({/* "isActive": true*/}, {/*'_id': 1, 'name': 1, 'description': 1, 'price': 1, 'isActive': 1, 'productImage': 1, 'category': 1*/})
		.then((result, error) => {
		if(result){
			return result;
		} else {
			return console.log(error);
		}		
	});
};

module.exports.getAllGPU = () => {

	return Product.find({ "category": "GPU"}, {/*'_id': 1, 'name': 1, 'description': 1, 'price': 1, 'isActive': 1, 'productImage': 1, 'category': 1*/})
		.then((result, error) => {
		if(result){
			return result;
		} else {
			return console.log(error);
		}		
	});
};

module.exports.getAllCPU = () => {

	return Product.find({ "category": "CPU"}, {/*'_id': 1, 'name': 1, 'description': 1, 'price': 1, 'isActive': 1, 'productImage': 1, 'category': 1*/})
		.then((result, error) => {
		if(result){
			return result;
		} else {
			return console.log(error);
		}		
	});
};

module.exports.getAllMOBO = () => {

	return Product.find({ "category": "MOBO"}, {/*'_id': 1, 'name': 1, 'description': 1, 'price': 1, 'isActive': 1, 'productImage': 1, 'category': 1*/})
		.then((result, error) => {
		if(result){
			return result;
		} else {
			return console.log(error);
		}		
	});
};




/*
module.exports.getAllProductMOBO = () => {

	return Product.find({ "category": "MOBO"}, {'_id': 1, 'name': 1, 'description': 1, 'price': 1, 'isActive': 1, 'productImage': 1})
		.then((result, error) => {
		if(result){
			return result;
		} else {
			return console.log(error);
		}		
	});
};

module.exports.getAllProductGPU = () => {

	return Product.find({ "isActive": false}, {'_id': 1, 'category': 1, 'name': 1, 'description': 1, 'price': 1, 'isActive': 1, 'productImage': 1})
		.then((result, error) => {
		if(result){
			return result;
		} else {
			return console.log(error);
		}		
	});
};*/


module.exports.getAllurl = (req, res) => {
   return Product.find({}, {'_id': 0, 'productImage': 1})
   .then((result, error) => {
		if(result){
			return result;
		} else {
			return console.log(error);
		}		
	});
};


/*TESTINGTABS*/
module.exports.detailstest = (userRole) => {
	return Product.find({}, {'_id': 1, 'name': 1, 'description': 1, 'price': 1})
		.then((result, error) => {
		if(result){
			return result;
		} else {
			return console.log(error);
		}		
	});
};
/*TESTINGTABS*/


/*GETSPECIFICPRODUCTBYID*/
module.exports.getSpecificProduct = (findId) => {

	return Product.findById(findId, {/*'_id': 0, 'name': 1, 'description': 1, 'price': 1, 'isActive': 1*/})
		.then((result, error) => {
		if(result){
			return result;
		} else {
			return console.log(error);
		}		
	});
	};



/*SetProduct to isActive = false/Archive*/
module.exports.ArchiveProduct = (params) => {
	const isActiveFalse = { isActive: false };

	return Product.findByIdAndUpdate(params.id, isActiveFalse)
	.then((result, error) => {
		return (error) ? false : true/*JSON.stringify('Product Archived!')*/;
	});
};

/*SetProduct to isActive = false/Archive*/


/*SetProduct to isActive = false/Archive*/
module.exports.ReArchiveProduct = (params) => {
	const isActiveTrue = { isActive: true };

	return Product.findByIdAndUpdate(params.id, isActiveTrue)
	.then((result, error) => {
		return (error) ? false : true/*JSON.stringify('Product Archived!')*/;
	});
};

/*SetProduct to isActive = false/Archive*/






/*UPDATINGAPRODUCT*/
module.exports.updateProduct = (productId, updateProduct) => {
return Product.findById(productId)
.then((result, error) => {
	if(error) {
		console.log(error);
		return false;
	} if(result.name === updateProduct.name) {
		return false/*JSON.stringify('Product Name Already in DB')*/;
	} else {

		result.name = updateProduct.name;
		result.description = updateProduct.description;
		result.price = updateProduct.price;
		result.category = updateProduct.category;
		result.highlightText = updateProduct.highlightText

		return result
		.save()
		.then((updatedTask, saveErr) => {
			if(saveErr) {
				console.log(saveErr);
				return false;
			} else {
				return true/*JSON.stringify('Product Updated')*/;
			}
		});
	};
});
};




/*SetProduct PHOTO*/
/*module.exports.setProductImage = (params) => {
	const isActiveFalse = { isActive: false };

	return Product.findByIdAndUpdate(findId, {$set: {'productImage': data }})
	.then((result, error) => {
		return (error) ? false : JSON.stringify('Product Archived!');
	});
};*/
/*SetProduct PHOTO*/


module.exports.setProductImage = (data, productId, updateProduct) => {
return Product.findById(productId)
.then((result, error) => {
	if(error) {
		console.log(error);
		return false;
	} else {

		result.productImage = updateProduct.data;


		return result
		.save()
		.then((updatedTask, saveErr) => {
			if(saveErr) {
				console.log(saveErr);
				return false;
			} else {
				return false/*JSON.stringify('Product Updated')*/;
			}
		});
	};
});
};




/*CREATECPU FOR CS3*/
/*module.exports.createProductCPU = async(params) => {


	let productCheck = await Product.findOne({ name: params.name})
	if(productCheck) return JSON.stringify("Product Already Exists in Db!");

	let cpuProduct = new Product({

		name: params.name,
		highlightText: params.highlightText,
		description: params.description,
		category: params.category,
		price: params.price,


	}); console.log(cpuProduct)
	return cpuProduct.save()
	.then((user, err) => {
		return (err) ? false : JSON.stringify(`${params.name} added to the DB!`);
	});
};
*/


