const User = require('../models/user');
/*const Product = require('../models/product');*/
const Order = require('../models/order');
const bcrypt = require('bcrypt');
const auth = require('../auth');

/*SOLO/*EMAILCHECKING*/
module.exports.emailExists = (body) => {
	return User.find({ email: body.email })
	.then(result => {
		return result.length > 0 ? true : false;
	});

};




//REGISTER
module.exports.register = async (params) => {

	/*EMAILCHECKING*/
	const emailExist = await User.findOne({email: params.email});
	if(emailExist) {
	 return JSON.stringify("Email Already Exists in Db!");
	} 
	/*EMAILCHECKING*/


	/*REGISTERNEWUSER*/
	let user = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		accessLevel: params.accessLevel,
		password: bcrypt.hashSync(params.password, 10)



	});
	return user.save()
	.then((user, err) => {
		return (err) ? false : true/*JSON.stringify("USER CREATED")*/;
	});
	
};
/*REGISTERNEWUSER*/

/*USERAUTHENTICATION*/
module.exports.login = (params) => {
	return User.findOne({ email: params.email })
	.then(user => {
		if (user === null) {
			return JSON.stringify("Email not Registered");
		} 
			const isPasswordMatched = bcrypt.compareSync(params.password, user.password);
		if (isPasswordMatched) {		
			return { accessToken: auth.createAccessToken(user.toObject())};
		} else {
			return JSON.stringify("Password and Email not matched");
		}

	});

};

/*SetUserAsAdmin*/
module.exports.SetUserAsAdmin = (findId) => {
return User.findByIdAndUpdate(findId, {$set: {'accessLevel': 'admin', 'isAdmin': true}})
.then((result, error) => {
	if (error){
			console.log(error);
			return false;
	} else {
		return true/*JSON.stringify((`'${findId}'s accountLevel set to 'Admin'!`))*/;
	}
});
};
/*SetUserAsAdmin*/
/*SetUserAsNOTAdmin CS3*/
module.exports.SetUserAsNOTAdmin = (findId) => {
return User.findByIdAndUpdate(findId, {$set: {'accessLevel': 'basic', 'isAdmin': false}})
.then((result, error) => {
	if (error){
			console.log(error);
			return false;
	} else {
		return true/*JSON.stringify((`'${findId}'s accountLevel set to 'Admin'!`))*/;
	}
});
};
/*SetUserAsNOTAdmin CS3*/


/*GETDEATILSUSERTESTONLY*/
module.exports.detailsUser = (findId) => {
	return User.findById(findId)
	.then((result, error) => {
	if (error){
			console.log(error);
			return false;
	} else {
		return result/*JSON.stringify((`'${findId}'s accountLevel set to 'Admin'!`))*/;
	}
});
};

module.exports.get = (params) => {
	return User.findById(params.userId)
	.then(user => {

		user.password = undefined;
		return user;
	});
};



/*UPDATEPROFILE CS#*/
module.exports.updateProfile = (productId, updateProduct) => {
return User.findById(productId)
.then((result, error) => {
	if(error) {
		console.log(error);
		return false;
	/*} if(result.name === updateProduct.name) {
		return falseJSON.stringify('Product Name Already in DB');*/
	} else {

		result.firstName = updateProduct.firstName;
		result.lastName = updateProduct.lastName;
		result.email = updateProduct.email;
		result.mobileNo = updateProduct.mobileNo;
		result.password = bcrypt.hashSync(updateProduct.password, 10)
		return result
		.save()
		.then((updatedTask, saveErr) => {
			if(saveErr) {
				console.log(saveErr);
				return false;
			} else {
				return true/*JSON.stringify('Product Updated')*/;
			}
		});
	};
});
};

