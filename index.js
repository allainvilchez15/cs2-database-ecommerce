//Set up the dependencies
const express = require('express');
const mongoose = require('mongoose');
const env = require('dotenv');
const userRoutes = require('./routes/userRoutes');
const productRoutes =  require('./routes/productRoutes');
const orderRoutes =  require('./routes/orderRoutes');
const cors = require('cors');
const inputValidation = require('joi');

env.config();

//database connection
mongoose.connect(
	process.env.DBCONNECTION,
{
	useNewUrlParser: true, 
	useUnifiedTopology: true,
	useFindAndModify: false
});

mongoose.connection.once('open', () => console.log('Now Connected to the Database'));


//server setup
const app = express();

app.use('/uploads', express.static('uploads'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


//routes
app.use('/', productRoutes);
app.use('/', userRoutes);
app.use('/', orderRoutes);

//server listening
/*app.listen(port, () => console.log(`Now listening at port: ${port}`));*/
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port: ${ process.env.PORT || 4000}`);
});

/*mongodb://admin:admin>@cluster0-shard-omitted.mongodb.net:27017,cluster0-shard-omitted.mongodb.net:27017,cluster0-shard-omitted.mongodb.net:27017/capstone2_database>?authSource=admin&ssl=true&retryWrites=true&w=majority*/