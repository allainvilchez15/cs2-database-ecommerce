const jwt = require('jsonwebtoken');
const secret = 'SecretCode';


//3 main parts
//1. creation of the token -> analogy: pack the gift, and sign with the secret

module.exports.createAccessToken = (user) => {
	//the data from the user parameter from the login
	
	const data = {
		firstName: user.firstName,
		id: user._id,
		email: user.email,
		accessLevel: user.accessLevel,
		isAdmin: true
	};
		//create the token and using the jwt sign function
	return jwt.sign(data, secret, {});
};

//2. verificatino of the token, anoalogy received and verify if the send is legitimate and the gift was not tampered.

 module.exports.verify = (request, response, next) => {
 	let token = request.headers.authorization;
 	if(typeof token !== 'undefined') {
 		token = token.slice(7, token.length);

 		//Recall slice function fo JS - cuts the string starting from the list value up to the specified value
 		//The first 7 characters is not relevant to the actual token, we dont need the word 'Bearer ', so we removed it using the slice 
 		//after the token has been sliced, we ethen use jwt's verfiy function
 		return jwt.verify(token, secret, (error, data) => {
 			return (error) ? response.send({ auth: 'failed', Login: 'error'}) : next();
 			//next() passes the request to the next callback function in the route
 			//next() tells the server to allow us to proceed with the next request
 			
 		});
 	} else {
 		return response.send({ auth: 'failed', Login: 'error'});
 	}
 };

 //3. decoding of the token -> analogy: open the gift and get the content
 
 module.exports.decode = (token) => {
 	//check if the token is present or not
 	if(typeof token !== 'undefined') {
 		token = token.slice(7, token.length);
 		return jwt.verify(token, secret, (error, data) => {
 			return (error) ? null : jwt.decode(token, { complete: true }).payload;
 			//{complete: true} grabs both the request header and the playload 
 		});
 	}
 };


//jwt.decode -> decodes the token and get the payload
//payload is the data from the token we create from createAccessToken
//the one with _id, the email, and isAdmin


/*module.exports.authPage = (permissions) => {
	return (req, res, next) => {
		const userRole = auth.decode(request.headers.authorization).role;
		if (permissions.includes(userRole)) {
			next();
		} else {
			return response.send('User Unauthorized');
		}
	}
};*/