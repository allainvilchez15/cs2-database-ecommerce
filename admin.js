const auth = require('./auth');


module.exports.authAdmin = (request, response, next) => {
	
		const userRole = auth.decode(request.headers.authorization).accessLevel;
		const isAdmin = auth.decode(request.headers.authorization).isAdmin;
	const adminOnly = 'admin';
if(userRole != null && userRole === adminOnly && isAdmin === true) {
	next();
	
		} else {
			return response.status(401).json({
				Access: "Denied!",
				message: "Login credentials not supported",

			});
		}
	};


/*const adminOnly = 'admin';
if(userRole != null && userRole === adminOnly) {
	next();
	*/