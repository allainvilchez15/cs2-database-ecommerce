const express = require('express');
const router = express.Router();
const UserController = require('../controllers/userController');
const AdminController = require('../controllers/adminController');
const auth = require('../auth');
const admin = require('../admin');
const {userValidateSchema, loginValidate} = require('../validation');


//to check for existing email

router.post('/emailcheck', (request, response) => {
	UserController.emailExists(request.body)
	.then(result => response.send(result));
});


//register a USER
router.post('/', (request, response) => {

/*	const {error} = userValidateSchema.validate(request.body);
	if (error) return response.status(400).json(error.details[0].message);*/
	UserController.register(request.body)
	.then(result => response.send(result));
});

router.post('/adminCreatePage',  (request, response) => {

	const {error} = userValidateSchema.validate(request.body);
	if (error) return response.status(400).json(error.details[0].message);
	AdminController.adminCreatePage(request.body)
	.then(result => response.send(result));
});

//login a user
router.post('/login',  (request, response) => {

	const {error} = loginValidate.validate(request.body);
	if (error) return response.status(400).json(error.details[0].message);
	UserController.login(request.body)
	.then(result => response.send(result));
});

//SetUserAsAdmin
router.put("/SetUserAsAdmin/:id", auth.verify, admin.authAdmin, (request, response) => {
	UserController.SetUserAsAdmin(request.params.id)
	.then(result => response.send(result));	
});

//SetUserAsAdmin
router.put("/SetUserAsNOTAdmin/:id", auth.verify, admin.authAdmin, (request, response) => {
	UserController.SetUserAsNOTAdmin(request.params.id)
	.then(result => response.send(result));	
});

//routes for getting the details of a user//TESTONLY
router.get('/detailsUser/:id', auth.verify, (request, response) => {
	UserController.detailsUser(request.params.id)
	.then(result => response.send(result));	
	/*const user = auth.decode(request.headers.authorization);
	UserController.detailsUser({ userId: user.id})*/
	/*.then(user => response.send(user));*/
});



//routes for getting the details of a user//TESTONLY
router.get('/detailsUser1', auth.verify, (request, response) => {
	const user = auth.decode(request.headers.authorization);
	UserController.get({ userId: user.id})
	.then(user => response.send(user));
});




//Route to get all the USERS /*ADDED FOR CS3*/ 
router.get("/getAllUsers", auth.verify, admin.authAdmin, (request, response) => {
	AdminController.getAllUsers(request.body)
	.then(result => response.send(result));	
});

/*ROUTE TO DELETE USER CS3*/
router.delete("/deleteUser/:id", auth.verify, admin.authAdmin, (request, response) => {
	AdminController.deleteUser(request.params.id)
	.then(result => response.send(result));	
});


//Route for Updating PRODUCT
router.put("/updateProfile/:id", auth.verify, /*admin.authAdmin, */(request, response) => {
	const {error} = userValidateSchema.validate(request.body);
	if (error) return response.status(400).json(error.details[0].message);
	UserController.updateProfile(request.params.id, request.body)
	.then(result => response.send(result));	
});





module.exports = router;