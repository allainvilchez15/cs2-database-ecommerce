const express = require('express');
const router = express.Router();
const OrderController = require('../controllers/orderController');
const AdminController = require('../controllers/adminController');
const auth = require('../auth');
const admin = require('../admin');
const {orderValidateSchema} = require('../validation');


//CREAATEORDER ROUTE
router.post("/createOrder", auth.verify, (request, response) => {

	const {error} = orderValidateSchema.validate(request.body);
	if (error) return response.status(400).json(error.details[0].message);

	let data = {
		userId: auth.decode(request.headers.authorization).id,
		//userId comes from the decoded JWT's id field
		productId: request.body.productId,//
		//courseId comes from the request body
		quantity: request.body.quantity,
	};
	OrderController.createOrder(data, request.body)
	.then(result => response.send(result));
});





//TEST222
router.get("/createOrderTest", auth.verify, (request, response) => {

	const {error} = orderValidateSchema.validate(request.body);
	if (error) return response.status(400).json(error.details[0].message);

	let data = {
		userId: auth.decode(request.headers.authorization).id,
		//userId comes from the decoded JWT's id field
		productId: request.body.productId,//
		//courseId comes from the request body
		quantity: request.body.quantity,
	};
	OrderController.createOrderTest(data, request.body)
	.then(result => response.send(result));
});


//Route to get SpecificORDERtbyID CS3
router.get("/getUsersSpecificOrder/:id", /*auth.verify,*/ (request, response) => {
	OrderController.getUsersSpecificOrder(request.params.id)
	.then(result => response.send(result));	
});



//GETUSERORDER
router.get("/getUsersOrder", auth.verify, (request, response) => {
		let data = {
		userId: auth.decode(request.headers.authorization).id,
	};
	OrderController.getUsersOrder(data)
	.then(result => response.send(result));
});



//GETALLORDES
router.get("/getAllOrders", auth.verify, admin.authAdmin, (request, response) => {
	let data = {
		userId: auth.decode(request.headers.authorization).id,
	};
	OrderController.getAllOrders(request.body)
	.then(result => response.send(result));	
});



//UPDATEORDER
router.put("/updateOrder/:id", auth.verify, (request, response) => {
	const {error} = orderValidateSchema.validate(request.body);
	if (error) return response.status(400).json(error.details[0].message);
	OrderController.updateOrder(request.params.id, request.body)
	.then(result => response.send(result));	
});





//ArchiveOrder
router.put("/ArchiveOrder/:id", auth.verify,/* admin.authAdmin,*/ (request, response) => {
	AdminController.ArchiveOrder(request.params.id)
	.then(result => response.send(result));	
});

//ReArchiveOrder
router.put("/ReArchiveOrder/:id", auth.verify, /*admin.authAdmin,*/ (request, response) => {
	AdminController.ReArchiveOrder(request.params.id)
	.then(result => response.send(result));	
});


/*ROUTE TO DELETE Proudt CS3*/
router.delete("/byeOrder/:id", auth.verify, /*admin.authAdmin,*/ (request, response) => {
	AdminController.deleteOrder(request.params.id)
	.then(result => response.send(result));	
});



//Route to get SpecificOrderbyID
router.get("/getSpecificOrder/:id", /*auth.verify,*/ (request, response) => {
	OrderController.getSpecificOrder(request.params.id)
	.then(result => response.send(result));	
});


module.exports = router;
