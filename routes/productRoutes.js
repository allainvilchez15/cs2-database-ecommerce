const express = require('express');
const router = express.Router();
const ProductController = require('../controllers/productController');
const AdminController = require('../controllers/adminController');
const auth = require('../auth');
const admin = require('../admin');
const {productValidateSchema} = require('../validation');

/*ADDEDFORUPLOADINGIMAGE*/
const fs = require('fs');
const multer = require('multer');
const storage = multer.diskStorage({
destination: function(request, file, callback) {
  fs.mkdir('./uploads/',(err)=>{
       callback(null, './uploads/');
    });
},
  filename: function(req, file, cb){
         cb(null, new Date().toISOString().replace(/:/g, '-') +'-'+ file.originalname);
}

});

const upload = multer({storage: storage});
/*ADDEDFORUPLOADINGIMAGE*/





//Route for Updating PRODUCT
router.get("/setProductImage/upload/", auth.verify, admin.authAdmin, upload.single('productImage'), (request, response) => {
/*	const {error} = productValidateSchema.validate(request.body);
	if (error) return response.status(400).json(error.details[0].message);
*/
	let data = `http://localhost:4000/${request.file.path}`
	

	ProductController.setProductImage(request.params.id, data, request.body)
	.then(result => response.send(result));	
});








//Route to create a PRODUCT CHANGETOCREATE GPU FOR CS3
router.post("/createProduct", auth.verify, admin.authAdmin, upload.single('productImage'), (request, response) => {
/*console.log(request.file)*/
	const {error} = productValidateSchema.validate(request.body);
	if (error) return response.status(400).json(error.details[0].message);

	let data = `http://localhost:4000/${request.file.path}`
	
	/*console.log(request.body)*/
	ProductController.createProduct(data, request.body)
	.then(result => response.send(result));
});




//Route to create a PRODUCT CHANGETOCREATE CPU FOR CS3
router.post("/createProductCPU", auth.verify, admin.authAdmin, (request, response) => {
/*console.log(request.file)*/
	const {error} = productValidateSchema.validate(request.body);
	if (error) return response.status(400).json(error.details[0].message);

	/*let data = `http://localhost:4000/${request.file.path}`*/
	
/*	console.log(data)*/
	ProductController.createProductCPU(request.body)
	.then(result => response.send(result));
});

//Route to create a PRODUCT CHANGETOCREATE MOBO FOR CS3
router.post("/createProductMOBO", auth.verify, admin.authAdmin, (request, response) => {
/*console.log(request.file)*/
	const {error} = productValidateSchema.validate(request.body);
	if (error) return response.status(400).json(error.details[0].message);

	ProductController.createProduct(request.body)
	.then(result => response.send(result));
});



//Route to get all the PRODUCT /*CHANGETOACTIVEONLY FOR CS3*/ REMOVE REQUIRED FOR LOGIN FOR CS3
router.get("/getAllProduct", /*auth.verify,*/ (request, response) => {
	ProductController.getAllProduct(request.body)
	.then(result => response.send(result));	
});


//Route to get all the PRODUCT /*CHANGETOACTIVEONLY FOR CS3*/ REMOVE REQUIRED FOR LOGIN FOR CS3
router.get("/getAllMOBO", /*auth.verify,*/ (request, response) => {
	ProductController.getAllMOBO(request.body)
	.then(result => response.send(result));	
});

router.get("/getAllGPU", /*auth.verify,*/ (request, response) => {
	ProductController.getAllGPU(request.body)
	.then(result => response.send(result));	
});

router.get("/getAllCPU", /*auth.verify,*/ (request, response) => {
	ProductController.getAllCPU(request.body)
	.then(result => response.send(result));	
});


router.get("/getAllurl", /*auth.verify,*/ (request, response) => {
	ProductController.getAllurl(request.body)
	.then(result => response.send(result));	
});



/*TEST*/ //CURRENT  ADMINLY
router.get("/detailstest",  auth.verify, admin.authAdmin,  (request, response) => {
	ProductController.detailstest(request.body)
	.then(result => response.send(result));	
});


//Route to get SpecificProductbyID
router.get("/getSpecificProduct/:id", /*auth.verify,*/ (request, response) => {
	ProductController.getSpecificProduct(request.params.id)
	.then(result => response.send(result));	
});

//Route for Updating PRODUCT
router.put("/updateProduct/:id", auth.verify, admin.authAdmin, (request, response) => {
	const {error} = productValidateSchema.validate(request.body);
	if (error) return response.status(400).json(error.details[0].message);
	ProductController.updateProduct(request.params.id, request.body)
	.then(result => response.send(result));	
});

//Route for Deleting/Archive  Product
router.delete("/ArchiveProduct/:id", auth.verify, admin.authAdmin, (request, response) => {
	ProductController.ArchiveProduct(request.params)
	.then(result => response.send(result));	
});


//Route for ReArchiveProduct Product
router.put("/ReArchiveProduct/:id", auth.verify, admin.authAdmin, (request, response) => {
	ProductController.ReArchiveProduct(request.params)
	.then(result => response.send(result));	
});


/*ROUTE TO DELETE Proudt CS3*/
router.delete("/byeProduct/:id", auth.verify, admin.authAdmin, (request, response) => {
	AdminController.deleteProduct(request.params.id)
	.then(result => response.send(result));	
});




module.exports = router;