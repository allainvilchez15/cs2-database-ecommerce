const inputValidation = require('joi');



const userValidateSchema = inputValidation.object({ 
	firstName: 
	inputValidation.string()
	.min(2)
	.required(),
	lastName: 
	inputValidation.string()
	.min(2)
	.required(),
	accessLevel: 
	inputValidation.string()
	.min(2),
	email: 
	inputValidation.string() 
	.min(5) 
	.required() 
	.email(),
	password: 
	inputValidation.string() 
	.min(6) 
	.required()
	.regex(/^[a-zA-Z0-9]{6,30}$/),
	mobileNo: 
	inputValidation.string()
	.min(3)
	.max(20)
	.required()
/*	.regex(/^[0-9]{8,20}$/)*/
	.messages({
		"string.pattern.base": "Mobile number must be composed of Digits!"
	}),
});


const productValidateSchema = inputValidation.object({ 
	name: 
	inputValidation.string()
	.min(2)
	.required(),
	highlightText: 
	inputValidation.string()
	.min(2)
	.required(),
	description: 
	inputValidation.string()
	.min(2)
	.required(),
	price: 
	inputValidation.number()
	.greater(5) 
	.min(1) 
	.required(),
	category: 
	inputValidation.string()
	.min(2)
	.required(),
	
});

const orderValidateSchema = inputValidation.object({ 
	productId: 
	inputValidation.string()
	.min(24)
	.required(),
	quantity:
	inputValidation.number()
	.min(1) 
	.required()
});

const loginValidate = inputValidation.object({ 
	email: 
	inputValidation.string()
	.min(5)
	.required()
	.email(),
	password: 
	inputValidation.string() 
	.min(6) 
	.required()
	.regex(/^[a-zA-Z0-9]{6,30}$/),
});




module.exports = { userValidateSchema, productValidateSchema, orderValidateSchema, loginValidate };
